/* eslint-disable func-names */
import {CLASSES, DOM} from '../_const';

const $btnOpen = $('.js-header-burger');
const $menu = $('.js-nav-drop');
const $menuWrapper = $('.js-nav-drop');

const menu = () => {
  $btnOpen.click((e) => {
    

    if ( !$menu.is('.openmn') ) {
      const $this = $(e.currentTarget);
      const $icon = $this.find('.burger-icon');

      $this.addClass(CLASSES.active);  
      $icon.toggleClass(CLASSES.open);
      $menu.toggleClass(CLASSES.active);  
      $('body').toggleClass('fix-mobile');  

      setTimeout(function () {       
         $menu.toggleClass('openmn');      
        }, 20);
    }
   
  } )


  const closeMenu = (e) => {
    if (!$menuWrapper.is(e.target) && !$btnOpen.is(e.target) && $menu.is('.openmn')) {  
      $menu.removeClass('openmn')
      $menu.removeClass(CLASSES.active);
      $btnOpen.find('.burger-icon').removeClass(CLASSES.open); 
      $btnOpen.removeClass(CLASSES.active);
      $('body').removeClass('fix-mobile');  
    }
  }

  DOM.$html.click(closeMenu)
}

export default menu;
