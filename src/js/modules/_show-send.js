/* eslint-disable no-shadow */
/* eslint-disable no-useless-escape */
/* eslint-disable no-undef */
import {CLASSES, DOM} from '../_const';

class BUY_POPUP {
  constructor (el) {
    this.$el = $(el);
    this.buySrc = this.$el.data('popup');
    this.$popup = $(`.js-popup[data-name-modal=${this.buySrc}]`);
    this.$btn = this.$el;
    this.init();
    this.close();
  }

  show(e) {
      e.preventDefault();
      this.$popup.addClass(CLASSES.active);
      this.$popup.addClass('darkness');
      DOM.$body.addClass(CLASSES.noScroll);
    
    
  } 

  closeBuy(e) {
    if (this.$popup.is(e.target)) {  
      this.$popup.removeClass(CLASSES.active); 
      this.$popup.removeClass('darkness');
      DOM.$body.removeClass(CLASSES.noScroll)
    }
  }

  close() {
    DOM.$html.click(this.closeBuy.bind(this));
  }

  init() {
    this.$btn.click(this.show.bind(this));
    
  }
}

export default BUY_POPUP;