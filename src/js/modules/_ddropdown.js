/* eslint-disable no-unused-vars */
/* eslint-disable eqeqeq */
/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-consts */
/* eslint-disable no-redeclare */
/* eslint-disable no-undef */
/* eslint-disable no-const */
/* eslint-disable consts-on-top */
/* eslint-disable no-use-before-define */
/* eslint-disable prefer-rest-params */
/* eslint-disable consistent-return */
/* eslint-disable func-names */
(function(e) {
    console.log(1);
    e.fn.ddslick = function(l) {
        if (c[l]) {
            return c[l].apply(this, Array.prototype.slice.call(arguments, 1))
        } 
            if (typeof l === "object" || !l) {
                return c.init.apply(this, arguments)
            } 
                e.error(`Method ${  l  } does not exists.`)
            
        
    };
    const c = {};
        const d = {
            data: [],
            keepJSONItemsOnTop: false,
            height: null,
            background: "#eee",
            selectText: "",
            defaultSelectedIndex: null,
            truncateDescription: true,
            imagePosition: "left",
            showSelectedHTML: true,
            clickOffToClose: true,
            embedCSS: true,
            onSelected() {}
        };
        const i = '<div class="dd-select"><input class="dd-selected-value" type="hidden" /><a class="dd-selected"></a><span class="dd-pointer dd-pointer-down"></span></div>';
        const a = '<ul class="dd-options"></ul>';
        const b = '<style id="css-ddslick" type="text/css">.dd-select{ border-radius:2px; width: 100% !important; position:relative; cursor:pointer;}.dd-desc { color:#aaa; display:block; overflow: hidden; font-weight:normal; line-height: 1.4em; }.dd-selected{ overflow:hidden; display:block;  font-weight:bold;}.dd-pointer{ width:0; height:0; position:absolute; right:10px; top:50%; margin-top:-3px;}.dd-pointer-down{ border:solid 5px transparent; border-top:solid 5px #000; }.dd-pointer-up{border:solid 5px transparent !important; border-bottom:solid 5px #000 !important; margin-top:-8px;}.dd-options{  border-top:none; list-style:none;  display:none; position:absolute; z-index:2000; margin:0; padding:0;background:#fff; overflow:auto;}.dd-option{   overflow:hidden; text-decoration:none;  cursor:pointer;-webkit-transition: all 0.25s ease-in-out; -moz-transition: all 0.25s ease-in-out;-o-transition: all 0.25s ease-in-out;-ms-transition: all 0.25s ease-in-out; }.dd-options > li:last-child > .dd-option{ border-bottom:none;}.dd-option:hover{ }.dd-selected-description-truncated { text-overflow: ellipsis; white-space:nowrap; }.dd-option-selected {  }.dd-option-image, .dd-selected-image { vertical-align:middle; float:left; margin-right:5px; max-width:64px;}.dd-image-right { float:right; margin-right:15px; margin-left:5px;}.dd-container{ width: 100% !important; position:relative;}​ .dd-selected-text { font-weight:bold}​</style>';
    c.init = function(l) {
        const l = e.extend({}, d, l);
        if (e("#css-ddslick").length <= 0 && l.embedCSS) {
            e(b).appendTo("head")
        }
        return this.each(function() {
            let p = e(this);
                const q = p.data("ddslick");
            if (!q) {
                const n = [];
                    const o = l.data;
                p.find("option").each(function() {
                    const w = e(this);
                        const v = w.data();
                    n.push({
                        text: e.trim(w.text()),
                        value: w.val(),
                        selected: w.is(":selected"),
                        description: v.description,
                        imageSrc: v.imagesrc
                    })
                });
                if (l.keepJSONItemsOnTop) {
                    e.merge(l.data, n)
                } else {
                    l.data = e.merge(n, l.data)
                }
                const m = p;
                    const s = e(`<div id="${  p.attr("id")  }"></div>`);
                p.replaceWith(s);
                p = s;
                p.addClass("dd-container").append(i).append(a);
                const n = p.find(".dd-select");
                    const u = p.find(".dd-options");
                u.css({
                    width: l.width
                });
                n.css({
                    width: l.width,
                    background: l.background
                });
                p.css({
                    width: l.width
                });
                if (l.height != null) {
                    u.css({
                        height: l.height,
                        overflow: "auto"
                    })
                }
                e.each(l.data, function(v, w) {
                    if (w.selected) {
                        l.defaultSelectedIndex = v
                    }
                    u.append(`<li><a class="dd-option">${  w.value ? ` <input class="dd-option-value" type="hidden" value="${  w.value  }" />` : ""  }${w.imageSrc ? ` <img class="dd-option-image${  l.imagePosition == "right" ? " dd-image-right" : ""  }" src="${  w.imageSrc  }" />` : ""  }${w.text ? ` <label class="dd-option-text">${  w.text  }</label>` : ""  }${w.description ? ` <small class="dd-option-description dd-desc">${  w.description  }</small>` : ""  }</a></li>`)
                });
                const t = {
                    settings: l,
                    original: m,
                    selectedIndex: -1,
                    selectedItem: null,
                    selectedData: null
                };
                p.data("ddslick", t);
                if (l.selectText.length > 0 && l.defaultSelectedIndex == null) {
                    p.find(".dd-selected").html(l.selectText)
                } else {
                    const r = (l.defaultSelectedIndex != null && l.defaultSelectedIndex >= 0 && l.defaultSelectedIndex < l.data.length) ? l.defaultSelectedIndex : 0;
                    j(p, r)
                }
                p.find(".dd-select").on("click.ddslick", function() {
                    f(p)
                });
                p.find(".dd-option").on("click.ddslick", function() {
                    j(p, e(this).closest("li").index())
                });
                if (l.clickOffToClose) {
                    u.addClass("dd-click-off-close");
                    p.on("click.ddslick", function(v) {
                        v.stopPropagation()
                    });
                    e("body").on("click", function() {
                        e(".dd-click-off-close").slideUp(50).siblings(".dd-select").find(".dd-pointer").removeClass("dd-pointer-up");
                        e(".dd-click-off-close").slideUp(50).closest(".dropdown-wrp").removeClass("active-sel");
                    })
                }
            }
        })
    };
    c.select = function(l) {
        return this.each(function() {
            if (l.index !== undefined) {
                j(e(this), l.index)
            }
        })
    };
    c.open = function() {
        return this.each(function() {
            const m = e(this);
                const l = m.data("ddslick");
            if (l) {
                f(m)
            }
        })
    };
    c.close = function() {
        return this.each(function() {
            const m = e(this);
                const l = m.data("ddslick");
            if (l) {
                k(m)
            }
        })
    };
    c.destroy = function() {
        return this.each(function() {
            const n = e(this);
                const m = n.data("ddslick");
            if (m) {
                const l = m.original;
                n.removeData("ddslick").unbind(".ddslick").replaceWith(l)
            }
        })
    };

    function j(q, s) {
        const u = q.data("ddslick");
        const r = q.find(".dd-selected");
            const n = r.siblings(".dd-selected-value");
            const v = q.find(".dd-options");
            const l = r.siblings(".dd-pointer");
            const p = q.find(".dd-option").eq(s);
            const m = p.closest("li");
            const o = u.settings;
            const t = u.settings.data[s];
        q.find(".dd-option").removeClass("dd-option-selected");
        p.addClass("dd-option-selected");
        u.selectedIndex = s;
        u.selectedItem = m;
        u.selectedData = t;
        if (o.showSelectedHTML) {
            r.html((t.imageSrc ? `<img class="dd-selected-image${  o.imagePosition == "right" ? " dd-image-right" : ""  }" src="${  t.imageSrc  }" />` : "") + (t.text ? `<label class="dd-selected-text">${  t.text  }</label>` : "") + (t.description ? `<small class="dd-selected-description dd-desc${  o.truncateDescription ? " dd-selected-description-truncated" : ""  }" >${  t.description  }</small>` : ""))
        } else {
            r.html(t.text)
        }
        n.val(t.value);
        u.original.val(t.value);
        q.data("ddslick", u);
        k(q);
        g(q);
        if (typeof o.onSelected === "function") {
            o.onSelected.call(this, u)
        }
    }

    function f(p) {
        const o = p.find(".dd-select");
            const m = o.siblings(".dd-options");
            const l = o.find(".dd-pointer");
            const n = m.is(":visible");
            r = o.closest(".dropdown-wrp");
        e(".dd-click-off-close").not(m).slideUp(50);
        e(".dd-pointer").removeClass("dd-pointer-up");
        r.removeClass("active-sel");
        if (n) {
            m.slideUp("fast");
            l.removeClass("dd-pointer-up");
            r.removeClass("active-sel");
        } else {
            m.slideDown("fast");
            l.addClass("dd-pointer-up");
            r.addClass("active-sel");
        }
        h(p)
    }

    function k(l) {
        l.find(".dd-options").slideUp(50);
        l.find(".dd-pointer").removeClass("dd-pointer-up").removeClass("dd-pointer-up");
        l.closest(".dropdown-wrp").removeClass("active-sel").removeClass("active-sel");
    }

    function g(o) {
        const n = o.find(".dd-select").css("height");
        const m = o.find(".dd-selected-description");
        const l = o.find(".dd-selected-image");
        if (m.length <= 0 && l.length > 0) {
            o.find(".dd-selected-text").css("lineHeight", n)
        }
    }

    function h(l) {
        // eslint-disable-next-line func-names
        l.find(".dd-option").each(function() {
            const p = e(this);
            const n = p.css("height");
            const o = p.find(".dd-option-description");
            const m = l.find(".dd-option-image");
            if (o.length <= 0 && m.length > 0) {
                p.find(".dd-option-text").css("lineHeight", n)
            }
        })
    }
})(jQuery);

