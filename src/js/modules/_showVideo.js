/* eslint-disable no-console */

import {CLASSES, DOM} from '../_const';

class YOUTUBE_POPUP {
  constructor (el) {
    this.$el = $(el);
    this.$popup = $('.js-popup-video');
    this.$iframe = $('#video-frame');
    this.$video = $('#video');
    this.$btn = this.$el;
    this.videoSrc = this.$el.data('video');
    this.init();
    this.close();
  }

  show() {
    if (this.videoSrc.indexOf('youtube') > -1) {
      this.showIframe();
    } else {
      this.showVideo();
    }
    this.$popup.addClass(CLASSES.active);
    DOM.$body.addClass(CLASSES.noScroll)
  }

  showVideo() {
    console.log('video');
    this.$iframe.attr('src', '');
    this.$video.attr('src', `./video/${this.videoSrc}` );
    this.$video.addClass(CLASSES.active);
  }

  showIframe() {
    console.log('frame video');
    this.$video.attr('src', '');
    this.$iframe.attr('src', `${this.videoSrc}?autoplay=1`);
    this.$iframe.addClass(CLASSES.active); 
  }

  closeVideo(e) {
    if (this.$popup.is(e.target) || $('.popup-video__main').is(e.target) ) {  
      this.$iframe.attr('src', '')
      this.$video.attr('src', '')
      this.$iframe.removeClass(CLASSES.active);
      this.$video.removeClass(CLASSES.active);
      this.$popup.removeClass(CLASSES.active);
      DOM.$body.removeClass(CLASSES.noScroll)
    }
  }

  close() {
    DOM.$html.click(this.closeVideo.bind(this));
  }

  init() {
    this.$btn.click(this.show.bind(this));
  }
}

export default YOUTUBE_POPUP;