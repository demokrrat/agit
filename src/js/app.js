/* eslint-disable vars-on-top */
/* eslint-disable spaced-comment */
/* eslint-disable no-undef */
/* eslint-disable no-console */
/* eslint-disable lines-around-directive */
/* eslint-disable no-var */
/* eslint-disable strict */
/* eslint-disable no-unused-expressions */
/* eslint-disable prefer-destructuring */
/* eslint-disable import/no-useless-path-segments */
/* eslint-disable import/no-unresolved */
/* eslint-disable import/order */
/* eslint-disable no-unused-vars */
/* eslint-disable func-names */
import {DOM, CLASSES} from './_const';
import menu from './modules/_menu'; 
import BUY_POPUP from './modules/_show-send';
import slick from './../../node_modules/slick-carousel/slick/slick';
import YOUTUBE_POPUP from './modules/_showVideo';

import './../../node_modules/jquery.maskedinput/src/jquery.maskedinput';
import './../../node_modules/jquery-validation/dist/jquery.validate';



import 'bootstrap';



document.addEventListener("DOMContentLoaded", function() {
  var div = document.querySelector('body')
  div.classList.add("loaded");
});

/*
window.onload = function() {
  var div = document.querySelector('body')
  div.classList.add("loaded");
}*/

const initBuy = () => {
  const $btn = $('.order-call-btn');

  $btn.each((index, element) => { 
    const buyElement = new BUY_POPUP (element);
    buyElement.init();
  });
  
  $( ".close-popup" ).on( "click", function() {
    $('.js-popup').removeClass(CLASSES.active);
    // $('.js-popup').removeClass('darkness');
    DOM.$body.removeClass(CLASSES.noScroll)
  });
}



const initVideos = () => {
    const $btn = $('.js-show-video');
  
    $btn.each((index, element) => {
      const videoElement = new YOUTUBE_POPUP (element);
      videoElement.init();
    });
  }


const onContentLoaded = () => {
  let lazyImages = [].slice.call(document.querySelectorAll('img.lazy'));

  const lazyLoad = () => {
    lazyImages.forEach((lazyImage) => {
      const topBreakpoint = lazyImage.classList.contains('lazy_last') ?
                            window.innerHeight + 500 : window.innerHeight + 200;

      if ((lazyImage.getBoundingClientRect().top <= topBreakpoint
          && lazyImage.getBoundingClientRect().bottom >= 0) && getComputedStyle(lazyImage).display !== 'none') {
        // eslint-disable-next-line
        lazyImage.src = lazyImage.dataset.src;
        // eslint-disable-next-line
        lazyImage.srcset = lazyImage.dataset.srcset;
        lazyImage.classList.remove('lazy');

        lazyImages = lazyImages.filter((image) => {
        return image !== lazyImage;
        });

        if (lazyImages.length === 0) {
          DOM.document.removeEventListener('scroll', lazyLoad);
          DOM.window.removeEventListener('resize', lazyLoad);
          DOM.window.removeEventListener('orientationchange', lazyLoad);
        }
      }
    });
  };

  DOM.doc.addEventListener('scroll', lazyLoad);
  DOM.win.addEventListener('resize', lazyLoad);
  DOM.win.addEventListener('orientationchange', lazyLoad);
}





const onLoad = () => {




  menu();
  initBuy();
  initVideos();

  $(document).ready(function ($) {

    $(".container-item").prepend(
       // '<div class="rw-slider-item item-transparent"></div>'
      );

      // слайдер для моб
      $(".det-cont-slider-mob").slick({
        dots: false,
        cssEase: 'linear',       
        speed: 700,     
        arrows: false,
        responsive: [         
          {
            breakpoint: 560,
            settings: {             
              adaptiveHeight: true
            } 
          }
        ]
      });

      $(".review-slider-det .btn-prev").on("click", function() {
        $('.det-cont-slider-mob').slick("slickPrev");
      });
  
      $(".review-slider-det .btn-next").on("click", function() {
        $('.det-cont-slider-mob').slick("slickNext");
      });

      // слайдер для моб
      $(".review-slider-mob").slick({
        dots: false,
        cssEase: 'linear',       
        prevArrow: ".nav-slider .btn-prev",
        nextArrow: ".nav-slider .btn-next",
        speed: 700,       
      });
    
      $(".slider-course").slick({
        dots: false,
        prevArrow: ".container-slider-course .btn-prev",
        nextArrow: ".container-slider-course .btn-next"
      });
    
      $(".review-slider").slick({
        dots: false,
       // fade: true,
        cssEase: 'linear',       
        prevArrow: ".container-review-slider .btn-prev",
        nextArrow: ".container-review-slider .btn-next",
        asNavFor: '.review-slider-top',
        speed: 700,
        responsive: [         
          {
            breakpoint: 560,
            /* settings: "unslick" */
            settings: {             
              adaptiveHeight: true
            } 
          }
        ]
      });

      $(".review-slider-top").slick({
        dots: false,
       // fade: true,
        cssEase: 'linear',             
        asNavFor: '.review-slider',
        speed: 700,
        arrows: false
      });
    
      // Отзывы на странице "Корпоративное обучение".
      $(".slider-det-cont").slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        cssEase: 'linear',
        dots: false,
        prevArrow: ".review-slider-det .btn-prev",
        nextArrow: ".review-slider-det .btn-next",
        responsive: [
          {
            breakpoint: 1920,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 880,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 560,
            /* settings: "unslick" */
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              variableWidth: false
            } 
          }
        ]
      });
     
      // // Отзывы на странице "Корпоративное обучение".
    
      $(".slider-mentors").slick({
        dots: false,
        prevArrow: ".wrap-slider-mentors .btn-prev",
        nextArrow: ".wrap-slider-mentors .btn-next"
      });

      // слайдер "менторы" страница о академии декс
      $(".mentors-slider-desk .mentors-slider-wrap").slick({
        dots: false,
        variableWidth: false,
        infinite: true,
        slidesToScroll: 1,
        slidesToShow: 1,
        prevArrow: ".mentors-about .btn-prev",
        nextArrow: ".mentors-about .btn-next"
      });

      $(".mentors-about .btn-prev").on("click", function() {
        $('.mentors-slider-desk .mentors-slider-wrap').slick("slickPrev");
      });
  
      $(".mentors-about .btn-next").on("click", function() {
        $('.mentors-slider-desk .mentors-slider-wrap').slick("slickNext");
      });

      // слайдер "менторы" страница о академии моб
      $(".mentors-slider-mob .mentors-slider-wrap").slick({
        dots: false,
        variableWidth: false,
        infinite: true,
        slidesToScroll: 1,
        slidesToShow: 3,
        prevArrow: ".mentors-about .btn-prev",
        nextArrow: ".mentors-about .btn-next",
        responsive: [         
          {
            breakpoint: 560,
            /* settings: "unslick" */
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1
            } 
          }
        ]
      });


      // слайдер курсы стилист
      $(".slid-stylist").slick({
        autoplay: true,
        // cssEase: 'linear',
        autoplaySpeed: 3000,
        speed: 1000,
 //       fade: true,
        dots: false,
        prevArrow: ".course-detail-slider .btn-prev",
        nextArrow: ".course-detail-slider .btn-next",
        infinite: true,
        centerMode: true,
        centerPadding: 0,
        variableWidth: false,
        variableHeight: false,
        slidesToScroll: 1,
        slidesToShow: 5,
        responsive: [
          {
            breakpoint: 1920,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1,              
            }
          },
          {
            breakpoint: 880,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 560,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
             // centerPadding: 40,
              variableWidth: true
            }
          }
        ]
      });

    
      // Слайдер "Хотите записаться на курсы?"
      $(".slider-questions, .slider-questions-ph").slick({
        autoplay: true,
        autoplaySpeed: 2000,
        cssEase: 'linear',
        // dots: true,
        infinite: true,
        centerMode: true,
        slidesToScroll: 1,
        slidesToShow: 5,
        prevArrow: "",
        nextArrow: "",
        responsive: [
          {
            breakpoint: 1920,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 880,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1,
              centerMode: false
            }
          },
          {
            breakpoint: 560,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1,
              centerMode: false
            }
          }
        ]
      });
      // Слайдер "Хотите записаться на курсы?"
    
      $('input[inputmode="tel"]').mask("+380(99) 99 99 999");
      $('input[type="tel"]').mask("+380(99) 99 99 999");
    
      $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
          const re = new RegExp(regexp);
          return this.optional(element) || re.test(value);
        },
        ""
      );
    
      $(".form-o-course").validate({
        rules: {
          "user-name": {
            required: true,
            minlength: 3,
            regex: "[A-Za-zА-Яа-яЁё]"
          },
          "user-phone": {
            required: true
          }
        },
        messages: {
          "user-name": {
            required: "",
            minlength: ""
          },
          "user-phone": {
            required: ""
          }
        }
      });
    
      $(".order-call-form").validate({
        rules: {
          "user-name": {
            required: true,
            minlength: 3,
            regex: "[A-Za-zА-Яа-яЁё]"
          },
          "user-phone": {
            required: true
          }
        },
        messages: {
          "user-name": {
            required: "",
            minlength: ""
          },
          "user-phone": {
            required: ""
          }
        }
      });
    
      $(".f-subscribe").validate({
        rules: {
          "s-field": {
            required: true,
            minlength: 3,
            regex: "[A-Za-zА-Яа-яЁё]"
          }
        },
        messages: {
          "s-field": {
            required: "",
            minlength: "",
            email: ""
          }
        }
      });
    
    /*  $(".list-teachers li").on("click", function() {
        $(".list-teachers li").removeClass("active");
        $(this).addClass("active");       
      }); */
    
      // adaptive nav
      const navOpenToggle = () => {
        document
          .querySelector(".site-header__right")
          .classList.toggle("site-header__right--open");
        document
          .querySelectorAll('[data-toggle="site-header__right"]')
          .forEach(elem => {
            elem.classList.toggle("site-header__right--open");
          });
      };
    
      const handlerNavBtn = () => {
        document.addEventListener("click", event => {
          let target = event.target;
          while (target !== this) {
            if (target.getAttribute("data-toggle") === "site-header__right") {
              navOpenToggle();
              return;
            }
            target = target.parentNode;
          }
        });
      };
    
      if (document.querySelector(".site-header__right")) {
        document
          .querySelector("body")
          .insertAdjacentHTML(
            "beforeEnd",
            '<div class="site-header__right-overlay" data-toggle="site-header__right"></div>'
          );
        handlerNavBtn();
      }
    
      // // adaptive nav

      $( ".training-prog-desk .subject-item .link" ).click(function(e) {
        e.preventDefault();
        const dataId = $(this).attr("data-id");
        const parent = $(this).parents('.col-md-4');
        const $submenu = $('.details-programs');
        parent.toggleClass('active');
        
        parent.siblings().removeClass('active');
        $submenu.siblings().removeClass('show');
    
        if(parent.hasClass('active')){
          $(`#${ dataId}`).addClass('show');
        }else{
          $(`#${ dataId}`).removeClass('show');
        }
        
      });

      $('.site-header__close-inner').click(function(e) {
        $('body').addClass('no-scroll');
      });

      $('.site-header__right-overlay ').click(function(e) {
        $('body').removeClass('no-scroll');
      });
      $('.site-header__close').click(function(e) {
        $('body').removeClass('no-scroll');
      });

      $( ".list-teachers li" ).click(function(e) {
        e.preventDefault();
        const dataId = $(this).attr("data-id");
        const parent = $(this).parents('.teachers');
        const $submenu = $('.teacher-view');

        $(".list-teachers li").removeClass("active");
        $submenu.siblings().removeClass('show');
        $(this).addClass("active"); 

    
        if($(this).hasClass('active')){
          $(`#${ dataId}`).addClass('show');
        }else{
          $(`#${ dataId}`).removeClass('show');
        }
        
      });

      $( ".mentors-slider-desk .mentor__item .show-info" ).click(function(e) {
        e.preventDefault();
        const dataId = $(this).attr("data-id");
        const $submenu = $('.mentors-about .r-section .info-wrap');
        const parent = $(this).parents('.mentor__item');
       
        parent.siblings().removeClass('active');
     //   $submenu.siblings('.info-wrap').hide( 500);
        $submenu.siblings('.info-wrap').fadeOut(300, "linear" );
        parent.addClass("active"); 

    
        if(parent.hasClass('active')){
          $(`#${ dataId}`).fadeIn(300, "linear");
         // $(`#${ dataId}`).show( 500);
          //$(`#${ dataId}`).addClass('showblock');
        }else{
          $(`#${ dataId}`).fadeOut(300, "linear");
        //  $(`#${ dataId}`).hide( 500);
         //$(`#${ dataId}`).removeClass('showblock');
        }
        
      });

      $( ".mentors-slider-mob .show-info" ).click(function(e) {
        e.preventDefault();
        const dataId = $(this).attr("data-id");
        const $submenu = $('.mentors-about .r-section .info-wrap');
        const parent = $(this).parents('.slick-slide');
       
        parent.siblings().removeClass('active');
     //   $submenu.siblings('.info-wrap').hide( 500);
        $submenu.siblings('.info-wrap').fadeOut(300, "linear" );
        parent.addClass("active"); 

    
        if(parent.hasClass('active')){
          $(`#${ dataId}`).fadeIn(300, "linear");
         // $(`#${ dataId}`).show( 500);
          //$(`#${ dataId}`).addClass('showblock');
        }else{
          $(`#${ dataId}`).fadeOut(300, "linear");
        //  $(`#${ dataId}`).hide( 500);
         //$(`#${ dataId}`).removeClass('showblock');
        }
        
      });

  
      $( ".btn-sign-show" ).click(function(e) {       
        e.preventDefault();      
        var id  = $(this).attr('href');    
        var top = $(id).offset().top;       
        
        $('body,html').animate({scrollTop: top}, 1500);
      });
      
	    $( ".slider-det-cont .js-show-video" ).click(function(e) {       
	       console.log('qwer');
	       
      });
      
/*

// // Отзывы на странице "Отзывы".
      $(window).bind("resize",function(){
        if($(this).width() < 767){
          $('.reviews-page').addClass('slide-review');
        }
        else{
          $('.reviews-page').removeClass('slide-review');
        }
      }).resize();

    
   
    
      $(".slide-review").slick({
        dots: false,
        prevArrow: ".reviews .btn-prev",
        nextArrow: ".reviews .btn-next"
      });
        */


        //модалка вывод инфы

      $( ".rev-modal-view .btn-def" ).click(function(e) {
        // e.preventDefault();
        const parent = $(this).parents('.rw-slider-item');   
        const dataName = $(this).parents('.rw-slider-item').find('.data-modal-name').attr("data-modal-name");
        const dataPost = $(this).parents('.rw-slider-item').find('.data-modal-post').attr("data-modal-post");
        const dataContent = $(this).parents('.rw-slider-item').find('.data-modal-content').attr("data-modal-content");
      
        parent.addClass('active');      
        

        if(parent.hasClass('active')){
          $(".review-modal .popup__wrapper h3").text(dataName);
          $(".review-modal .popup__wrapper p").text(dataPost);
          $(".review-modal .popup__wrapper .review-content").text(dataContent);
          $(".review-modal").addClass('is-active darkness');
          $('body').addClass('no-scroll');
        }else{
          $(".review-modal .popup__wrapper h3").detach();
          $(".review-modal .popup__wrapper p").detach();
          $(".review-modal .popup__wrapper .review-content").detach();
          $(".review-modal").removeClass('is-active darkness');
          $('body').removeClass('no-scroll');
        }

      });

   

      $('.tabs-mob .nav-link').click(function(e) {		
        e.preventDefault();
        var item = $(this).closest('.nav-item');
        var text = $(this).siblings('.tab-pane');
        var isActive = item.hasClass('active');
        
        if ( isActive ) {
          item.removeClass('active');
          text.slideUp();
        } else {
          item.addClass('active');
          text.slideDown();
          item.siblings('.nav-item.active').removeClass('active').find('.tab-pane').slideUp();
        }
        return false;
      });

      $('.teachers-mob .teachers-link').click(function(e) {		
        e.preventDefault();
        var item = $(this).closest('.teachers-item');
        var text = $(this).siblings('.teacher-view');
        var isActive = item.hasClass('active');
        

        if ( isActive ) {
          item.removeClass('active');
          text.slideUp();
        } else {
          item.addClass('active');
          text.slideDown();
          item.siblings('.teachers-item.active').removeClass('active').find('.teacher-view').slideUp();
        }
        return false;
      });

      $('.teachers-item:first > .teacher-view').slideDown();

      $('.teachers-mob .teachers-item:first').addClass('active');
     

      $('.payment-tabs-mob .nav-link').click(function(e) {		
        e.preventDefault();
        var item = $(this).closest('.nav-item');
        var text = $(this).siblings('.tab-pane');
        var isActive = item.hasClass('active');
        
        if ( isActive ) {
          item.removeClass('active');
          text.slideUp();
        } else {
          item.addClass('active');
          text.slideDown();
          item.siblings('.nav-item.active').removeClass('active').find('.tab-pane').slideUp();
        }
        return false;
      });

      $('.training-prog-mob .subject-item .link').click(function(e) {		
        e.preventDefault();
        var item = $(this).closest('.col-md-4');
        var text = $(this).parents('.subject-item').siblings('.details-programs');
        var isActive = item.hasClass('active');

       // var destination = $('.col-md-4.active').offset().top;
        


        
        if ( isActive ) {
          item.removeClass('active');
          text.slideUp();          
        } else {
          item.addClass('active');
          text.slideDown();
          item.siblings('.col-md-4.active').removeClass('active').find('.details-programs').slideUp();
         // $("html:not(:animated),body:not(:animated)").animate({scrollTop: $('.col-md-4.active').offset().top}, 800);
        }
        return false;
      });

});

// ------------------------
// open modal
// ------------------------

// $('.order-call-btn').modal('show');
  // mainThumbsSlider.init();
  
  // modelsSliderContentIn.init();
  // mainThumbsContent.init();
  // modelsSliderContentInThumbs.init();

}



$(DOM.doc).ready(onLoad);
DOM.doc.addEventListener('DOMContentLoaded', onContentLoaded);

